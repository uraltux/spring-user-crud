package com.kost.crud.service.Impl;

import com.kost.crud.model.Role;
import com.kost.crud.model.User;
import com.kost.crud.repo.UserRepo;
import com.kost.crud.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

import static com.kost.crud.model.Role.ADMIN;

@SpringBootTest
@Slf4j
class UserServiceImplTest {
    @Autowired
    UserService userService;
    @Autowired
    UserRepo userRepo;

    @Test
    void addDuplicateUser() {
        Set<Role> set = Arrays.stream(Role.values()).collect(Collectors.toSet());
        User user = new User("ivan", "qwe@mail.ru", "123", set);
        userService.addUser(user);
        User user1 = new User("ivan", "qwe@mail.ru", "1211233", set);
        try {

            userService.addUser(user1);
            log.info("пользователь с дублем добавлен");
        } catch (Exception e) {
            log.info("дубликат не добавлен {}", e.getLocalizedMessage());
        }
    }

    @Test
    void addUser() {
        Set<Role> roles = new HashSet<>();
        roles.add(ADMIN);
        User user = new User("kostya", "uraltux@gmail.com", "1234556", roles);
        userService.addUser(user);
        User dbUser = userService.getUserByEmail("uraltux@gmail.com");
        Assertions.assertEquals(user, dbUser);
    }

    @Test
    void editUser() {
        Set<Role> roles = new HashSet<>();
        roles.add(ADMIN);
        User user = new User("kostya", "uraltux@gmail.com", "1234556", roles);
        userService.addUser(user);
        User dbUser = userService.getUserByEmail("uraltux@gmail.com");
        dbUser.setEmail("urallinux@gmail.com");
        userService.editUser(dbUser);
        Assertions.assertFalse(dbUser.equals(user));
    }

    @Test
    void removeUser() {
        Set<Role> roles = new HashSet<>();
        roles.add(ADMIN);
        User user = new User("kostya", "uraltux@gmail.com", "1234556", roles);
        userService.addUser(user);
        userService.removeUser(user.getId());
        User dbUser = userService.getUserById(user.getId());
        Assertions.assertEquals(null, dbUser);

    }

}