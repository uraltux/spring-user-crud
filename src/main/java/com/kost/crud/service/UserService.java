package com.kost.crud.service;

import com.kost.crud.model.User;

public interface UserService {
    void addUser(User user);

    void editUser(User user);

    void removeUser(Long id);

    User getUserById(Long id);

    User getUserByEmail(String email);

    User getUserByName(String name);
}
