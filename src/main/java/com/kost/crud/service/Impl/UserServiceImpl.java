package com.kost.crud.service.Impl;

import com.kost.crud.model.User;
import com.kost.crud.repo.UserRepo;
import com.kost.crud.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class UserServiceImpl implements UserService {
    private final UserRepo userRepo;

    public UserServiceImpl(UserRepo userRepo) {
        this.userRepo = userRepo;
    }

    @Override
    public void addUser(User user) {
        try {
            userRepo.save(user);
            log.info("Добавлен новый пользователь {}", user);

        } catch (Exception e) {
            log.info("Невозможно добавить пользователя. Ошибка = {} ", e.getMessage());
        }
    }

    @Override
    public void editUser(User user) {
        User dbUser = userRepo.findById(user.getId()).orElse(null);
        if (dbUser != null) {
            userRepo.save(user);
            log.info("Редактирование пользователя {}", dbUser);
        }
    }

    @Override
    public void removeUser(Long id) {
        userRepo.deleteById(id);
        log.info("Удален пользователь с ID = {}", id);

    }

    @Override
    public User getUserById(Long id) {
        log.info("Поиск пользователя по ID = {}", id);
        return userRepo.findById(id).orElse(null);

    }

    @Override
    public User getUserByEmail(String email) {
        log.info("Поиск пользователя по Email: {}", email);
        return userRepo.findUserByEmail(email);
    }

    @Override
    public User getUserByName(String name) {
        log.info("Поиск пользователя по Имени: {}", name);
        return userRepo.findUserByName(name);
    }
}
