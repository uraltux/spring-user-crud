package com.kost.crud.model;

public enum Role {
    ADMIN, USER, SUPER_ADMIN
}
