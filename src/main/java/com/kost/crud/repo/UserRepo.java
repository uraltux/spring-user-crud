package com.kost.crud.repo;

import com.kost.crud.model.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepo extends JpaRepository<User, Long> {

    User findUserByName(String name);

    User findUserByEmail(String email);

}
